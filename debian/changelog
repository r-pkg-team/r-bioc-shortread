r-bioc-shortread (1.64.0-3) unstable; urgency=medium

  * Team upload.
  * Skip autopkgtests on riscv64.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 27 Jan 2025 13:33:39 +0100

r-bioc-shortread (1.64.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 15:30:02 +0100

r-bioc-shortread (1.64.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  Set upstream metadata fields: Archive.

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 08 Nov 2024 11:33:42 +0100

r-bioc-shortread (1.62.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 11:48:58 +0200

r-bioc-shortread (1.62.0-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 23 Jul 2024 12:17:21 +0200

r-bioc-shortread (1.62.0-1~0exp) experimental; urgency=medium

  * Team upload
  * New upstream version. Closes: #1071350
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 09 Jul 2024 21:13:33 +0200

r-bioc-shortread (1.60.0-2) unstable; urgency=medium

  * Delete one test file also on armel

 -- Andreas Tille <tille@debian.org>  Mon, 18 Dec 2023 07:49:58 +0100

r-bioc-shortread (1.60.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 18:15:12 +0100

r-bioc-shortread (1.58.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 26 Jul 2023 22:39:54 +0200

r-bioc-shortread (1.56.1-1) unstable; urgency=medium

  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 16:34:41 +0100

r-bioc-shortread (1.54.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 16 May 2022 07:29:08 +0200

r-bioc-shortread (1.52.0-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2021 14:55:23 +0100

r-bioc-shortread (1.50.0-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * d/u/metadata: Completed references to registries
  * d/rules: Cleaning files auto-generated during build

  [ Andreas Tille ]
  * Provide autopkgtest-pkg-r.conf to make sure Test-Depends will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Tue, 07 Sep 2021 12:13:45 +0200

r-bioc-shortread (1.48.0-2) unstable; urgency=medium

  * Exclude failing test on non-Intel architectures
    Closes: #980082

 -- Andreas Tille <tille@debian.org>  Sun, 07 Feb 2021 12:28:32 +0100

r-bioc-shortread (1.48.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 02 Nov 2020 14:04:34 +0100

r-bioc-shortread (1.46.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 20 May 2020 14:28:26 +0200

r-bioc-shortread (1.44.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0

 -- Dylan Aïssi <daissi@debian.org>  Fri, 07 Feb 2020 20:50:24 +0100

r-bioc-shortread (1.44.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 28 Dec 2019 22:25:53 +0100

r-bioc-shortread (1.44.0-1) unstable; urgency=medium

  * New upstream version
  * Fixed debian/watch for BioConductor
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * debian/copyright: Use spaces rather than tabs in continuation lines.

 -- Andreas Tille <tille@debian.org>  Mon, 11 Nov 2019 11:46:22 +0100

r-bioc-shortread (1.42.0-1) unstable; urgency=medium

  [ Dylan Aïssi ]
  * Remove patch and use r-bioc-zlibbioc.

  [ Andreas Tille ]
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Fri, 19 Jul 2019 10:45:56 +0200

r-bioc-shortread (1.40.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Mon, 12 Nov 2018 08:02:46 +0100

r-bioc-shortread (1.38.0-2) unstable; urgency=medium

  * Fix Maintainer and Vcs-fields

 -- Andreas Tille <tille@debian.org>  Sun, 06 May 2018 08:02:34 +0200

r-bioc-shortread (1.38.0-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Thu, 03 May 2018 14:32:53 +0200

r-bioc-shortread (1.36.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.3
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Tue, 13 Mar 2018 08:50:11 +0100

r-bioc-shortread (1.36.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 09 Nov 2017 08:46:50 +0100

r-bioc-shortread (1.34.2-1) unstable; urgency=medium

  * New upstream version
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Sun, 22 Oct 2017 19:30:51 +0200

r-bioc-shortread (1.34.1-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * Add debian/README.source to document binary data files

 -- Andreas Tille <tille@debian.org>  Tue, 03 Oct 2017 00:04:38 +0200

r-bioc-shortread (1.34.1-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * Standards-Version: 4.1.0 (no changes needed)
  * Drop unused lintian overrides

 -- Andreas Tille <tille@debian.org>  Sat, 02 Sep 2017 23:56:24 +0200

r-bioc-shortread (1.34.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version
  * Add Build-Depends: r-bioc-rsamtools (>= 1.28.0)

 -- Graham Inggs <ginggs@debian.org>  Mon, 29 May 2017 15:13:59 +0200

r-bioc-shortread (1.32.0-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Generic BioConductor homepage

 -- Andreas Tille <tille@debian.org>  Wed, 26 Oct 2016 17:08:54 +0200

r-bioc-shortread (1.30.0-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Mon, 09 May 2016 10:58:41 +0200

r-bioc-shortread (1.28.0-1) unstable; urgency=medium

  * New upstream version
  * Ignore false positive lintian warnings

 -- Andreas Tille <tille@debian.org>  Fri, 06 Nov 2015 08:59:35 +0100

r-bioc-shortread (1.26.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #789985

 -- Andreas Tille <tille@debian.org>  Sat, 27 Jun 2015 19:18:04 +0200

r-bioc-shortread (1.24.0-1) experimental; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * adapt testsuite

 -- Andreas Tille <tille@debian.org>  Tue, 21 Oct 2014 21:21:30 +0200

r-bioc-shortread (1.22.0-1) unstable; urgency=medium

  * New upstream version
  * Moved debian/upstream to debian/upstream/metadata
  * Add autopkgtest
  * (Build-)Depends: r-bioc-genomicalignments
    Closes: #753224

 -- Andreas Tille <tille@debian.org>  Thu, 24 Jul 2014 11:38:30 +0200

r-bioc-shortread (1.20.0-1) unstable; urgency=low

  * Initial release (closes: #731085)

 -- Andreas Tille <tille@debian.org>  Mon, 04 Nov 2013 17:24:35 +0100
