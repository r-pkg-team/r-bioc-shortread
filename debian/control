Source: r-bioc-shortread
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-biocgenerics,
               r-bioc-biocparallel,
               r-bioc-biostrings,
               r-bioc-rsamtools,
               r-bioc-genomicalignments,
               r-bioc-biobase,
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-bioc-genomeinfodb,
               r-bioc-genomicranges,
               r-bioc-pwalign,
               r-cran-hwriter,
               r-cran-lattice,
               r-cran-latticeextra,
               r-bioc-xvector,
               r-bioc-rhtslib,
               architecture-is-64-bit
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-shortread
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-shortread.git
Homepage: https://bioconductor.org/packages/ShortRead/
Rules-Requires-Root: no

Package: r-bioc-shortread
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R classes and methods for high-throughput short-read sequencing data
 This BioConductor module is a package for input, quality assessment,
 manipulation and output of high-throughput sequencing data. ShortRead is
 provided in the R and Bioconductor environments, allowing ready access
 to additional facilities for advanced statistical analysis, data
 transformation, visualization and integration with diverse genomic
 resources.
